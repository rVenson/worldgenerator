extends Spatial

var next_point : Vector2
var direction : Vector2

func get_next_point():
	next_point = Vector2(rand_range(-10, 10), rand_range(-10, 10))
	next_point.x += transform.origin.x
	next_point.y += transform.origin.z
	
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(
		Vector3(next_point.x, 50, next_point.y),
		Vector3(next_point.x, -50, next_point.y))
	if result && !result.collider.is_in_group("ground"):
		get_next_point()
	
	#print(next_point)

func _ready():
	get_next_point()

func _physics_process(delta):
	var position = Vector2(transform.origin.x, transform.origin.z)
	if position.distance_to(next_point) < 1.0:
		get_next_point()
	direction = position.direction_to(next_point)
	var look_at_direction = Vector3(next_point.x, transform.origin.y, next_point.y)
	look_at(look_at_direction, Vector3.UP)
	
	# RAYCAST
	if $RayCast.is_colliding():
		transform.origin.x += direction.x * delta
		transform.origin.z += direction.y * delta
		transform.origin.y = $RayCast.get_collision_point().y
		if !$AnimationPlayer.is_playing():
			$AnimationPlayer.play("moving")
