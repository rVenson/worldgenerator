extends Popup

var world
var options

func _ready():
	world = get_node("/root/WorldGenerator")
	
	$VBoxContainer/OptionMapSize/HSlider.value = world.map_size
	$VBoxContainer/OptionLandWater/HSlider.value = world.land_water
	$VBoxContainer/OptionLandHeight/HSlider.value = world.land_height
	
func _process(delta):
	$VBoxContainer/OptionMapSize/Value.text = str($VBoxContainer/OptionMapSize/HSlider.value)
	$VBoxContainer/OptionLandWater/Value.text = str($VBoxContainer/OptionLandWater/HSlider.value)
	$VBoxContainer/OptionLandHeight/Value.text = str($VBoxContainer/OptionLandHeight/HSlider.value)

func _on_RegenerateButton_pressed():
	var options = {
		"map_size": $VBoxContainer/OptionMapSize/HSlider.value,
		"land_water": $VBoxContainer/OptionLandWater/HSlider.value,
		"land_height": $VBoxContainer/OptionLandHeight/HSlider.value
	}
	
	world.regenerate_world(options)
	visible = false


func _on_Button_pressed():
	popup_centered()
