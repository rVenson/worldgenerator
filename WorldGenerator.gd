extends Spatial

export var cache_folder = "res://MapCache/"
export var map_size = 100
export var land_water = 5.0
export var land_height = 30
export var seed_key = ""
export var shader = {
	"grass": preload("res://grass_shader.material"),
	"water": preload("res://WaterShader.tres")
}
var assets = {
	"tree" : [
		preload("res://tree_lowpoly2.tscn"), 
		preload("res://tree_lowpoly.tscn")
	],
	"rock" : preload("res://rock.tscn"),
	"grass" : preload("res://Grass2.tscn"),
}

var noise
var map : Spatial

var actual_position = Vector2(0, 0)
var loaded_chunks = Array()
var loading_chunks = Array()
var unloading_chunks = Array()

var thread : Thread
var load_time = 0

onready var UI = get_node("/root/WorldGenerator/ViewportContainer/Viewport/UI")

func _ready():
	_generate_noise()
	_append_map_node()
	_start_chunk_loader()

func _process(delta):
	_update_GUI()

func _flush_chunks():
	_generate_noise()
	
	# TODO: stop thread before kill and flush loadings
	#_start_chunk_loader()
	
	loaded_chunks.clear()
	loading_chunks.clear()
	unloading_chunks.clear()
	
	for child in map.get_children():
		map.remove_child(child)
		child.queue_free()

func remove_file_cache():
	var fs = Directory.new()
	fs.change_dir(cache_folder)
	fs.list_dir_begin(true, true)
	var file_name = fs.get_next()
	while(file_name != ""):
		fs.remove(cache_folder + file_name)
		file_name = fs.get_next()
	fs.list_dir_end()

func regenerate_world(options):
	
	self.map_size = options.map_size
	self.land_water = options.land_water
	self.land_height = options.land_height
	
	print_debug(options)
	
	_flush_chunks()
	
	$Player.transform.origin.x = 0
	$Player.transform.origin.z = 0
	
	remove_file_cache()
	
	spawn_chunk_block(Vector2(0, 0), 3)

func _update_GUI():
	UI.get_node("VBoxContainer/ChunksLabel").text = "Number of Chunks Loaded: " + str(loaded_chunks.size())
	UI.get_node("VBoxContainer/LoadingChunksLabel").text = "Loading Chunks: " + str(loading_chunks.size())
	UI.get_node("VBoxContainer/UnloadingChunksLabel").text = "UnLoading Chunks: " + str(unloading_chunks.size())
	UI.get_node("VBoxContainer/LoadTime").text = "Last Load Time: " + str(load_time)
	UI.get_node("VBoxContainer/ThreadStatus").text = "Thread Status: " + str(thread.is_active())
	UI.get_node("VBoxContainer/FPS").text = "FPS: " + str(Engine.get_frames_per_second())
	UI.get_node("Panel/Label").text = str(loaded_chunks)
	UI.get_node("VBoxContainer/MapInstances").text = "Map Instances: " + str(map.get_child_count())
	pass

func _start_chunk_loader():
	thread = Thread.new()
	var error = thread.start(self, "chunk_loader", null, Thread.PRIORITY_LOW)

func _append_map_node():
	map = Spatial.new()
	map.name = "Map"
	add_child(map)

func _generate_water(water_instance : MeshInstance, mesh_instance : MeshInstance):
	var plane_mesh = create_plane(map_size, 2)
	
	var surface_tool = SurfaceTool.new()
	surface_tool.create_from(plane_mesh, 0)
	var array_plane = surface_tool.commit()
	var data_tool = MeshDataTool.new()
	data_tool.create_from_surface(array_plane, 0)
	
	# COMMIT
	data_tool.commit_to_surface(array_plane)
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface_tool.create_from(array_plane, 0)
	
	water_instance.mesh = surface_tool.commit()
	water_instance.set_surface_material(0, shader.water)
	
	# CREATE MESH COLLISION
	append_mesh_collision(water_instance)

func append_mesh_collision(mesh_instance):
	var static_body = StaticBody.new()
	mesh_instance.add_child(static_body)
	static_body.set_owner(mesh_instance)
	var collision_mesh = CollisionShape.new()
	collision_mesh.shape = mesh_instance.mesh.create_trimesh_shape()
	static_body.add_child(collision_mesh)
	collision_mesh.set_owner(mesh_instance)
	
	return static_body

func _append_asset(vertex : Vector3, type : String):
	var obj : Node
	if type == 'tree':
		if randf() > 0.5:
			obj = assets.tree[0].instance()
		else:
			obj = assets.tree[1].instance()
		obj.transform.origin = Vector3(vertex.x, vertex.y - 0.2, vertex.z)
		obj.rotation = Vector3(0, randi(), 0)
		var scale = randf() + 0.5
		obj.scale = Vector3(scale, scale, scale)
	else:
		if type == 'rock':
			obj = assets.rock.instance()
			obj.rotation = Vector3(0, randi(), 0)
			var scale = randf() * 2 + 0.2
			obj.transform.origin = Vector3(vertex.x, vertex.y + scale/3 - scale/2, vertex.z)
			obj.scale = Vector3(scale, scale, scale)
		else:
			if type == 'grass':
				obj = assets.grass.instance()
				obj.rotation = Vector3(0, randi(), 0)
				obj.transform.origin = vertex
	
	return obj

func create_plane(size : int, surface_division: int):
	var plane_mesh = PlaneMesh.new()
	plane_mesh.size = Vector2(size, size)
	plane_mesh.subdivide_depth = size / surface_division
	plane_mesh.subdivide_width = size / surface_division
	return plane_mesh

func _generate_chunk_mesh(mesh_instance : MeshInstance, position : Vector2):
	# GENERATE THE PLANE
	var plane_mesh = create_plane(map_size, 4)
	
	# EDIT
	var surface_tool = SurfaceTool.new()
	surface_tool.create_from(plane_mesh, 0)
	var array_plane = surface_tool.commit()
	var data_tool = MeshDataTool.new()
	data_tool.create_from_surface(array_plane, 0)
	
	# GENERATE THE OBJECTS PARENT
	var objects_node = Spatial.new()
	objects_node.name = "Objects"
	mesh_instance.add_child(objects_node)
	objects_node.set_owner(mesh_instance)
	
	# GENERATE MESH
	for i in range(data_tool.get_vertex_count()):
		var vertex = data_tool.get_vertex(i)
		
		vertex.y = (noise.low.get_noise_3d(vertex.x + (position.y * map_size), vertex.y, vertex.z  + (position.x * map_size)) * land_height) + land_water
		vertex.y *= noise.high.get_noise_3d(vertex.x + (position.y * map_size), vertex.y, vertex.z  + (position.x * map_size)) + 1
		
		# Apply fallof_map
#		var fallof = 8
#		var x = vertex.x / fallof
#		var z = vertex.z / fallof
#		var value = max(abs(x), abs(z))
#		vertex.y -= value
		
		# CUT WATER EDGES
		if(vertex.y > -0.5 && vertex.y < 0.5):
			vertex.y -= 0.7
		
		# POPULATE
		var obj : Node
		if noise.tree.get_noise_3d(vertex.x + (position.y * map_size), vertex.y,  vertex.z + (position.y * map_size)) > 0.6:
			if vertex.y > 4.0 && vertex.y < 15.0:
				obj = _append_asset(vertex, "tree")
				objects_node.add_child(obj)
				obj.set_owner(mesh_instance)
			else:
				if noise.tree.get_noise_3d(vertex.x + (position.y * map_size), vertex.y,  vertex.z + (position.y * map_size)) > 0.68:
					if vertex.y > 0.5:
						obj = _append_asset(vertex, "rock")
						objects_node.add_child(obj)
						obj.set_owner(mesh_instance)
		
		if noise.grass.get_noise_3d(vertex.x + (position.y * map_size), vertex.y,  vertex.z + (position.y * map_size)) > 0.4:
			if vertex.y > 2.0 && vertex.y < 15.0:
				obj = _append_asset(vertex, "grass")
				objects_node.add_child(obj)
				obj.set_owner(mesh_instance)
		
		data_tool.set_vertex(i, vertex)
	
	for i in range(array_plane.get_surface_count()):
		array_plane.surface_remove(i)
	
	# COMMIT
	data_tool.commit_to_surface(array_plane)
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLE_FAN)
	surface_tool.create_from(array_plane, 0)
	surface_tool.generate_normals()
	
	mesh_instance.mesh = surface_tool.commit()

	# CREATE MESH COLLISION
	var static_body = append_mesh_collision(mesh_instance)
	static_body.add_to_group("ground", true)
	static_body.set_collision_layer_bit(1, true)
	
	# SET SURFACE MATERIAL
	mesh_instance.set_surface_material(0, shader.grass)
	
	# GEN WATER BLOCK
	var water_instance = MeshInstance.new()
	mesh_instance.add_child(water_instance)
	water_instance.set_owner(mesh_instance)
	_generate_water(water_instance, mesh_instance)
	
	# GEN NAVMESH
#	var nav = Navigation.new()
#	mesh_instance.add_child(nav)
#	nav.set_owner(mesh_instance)
#
#	var navmesh = NavigationMesh.new()
#	navmesh.create_from_mesh(mesh_instance.mesh)
#
#	var navmesh_instance = NavigationMeshInstance.new()
#	navmesh_instance.navmesh = navmesh
#	nav.add_child(navmesh_instance)
#	navmesh_instance.set_owner(mesh_instance)

	return mesh_instance

func _generate_noise():
	# NOISE
	noise = {
		'low': OpenSimplexNoise.new(),
		'high': OpenSimplexNoise.new(),
		'tree': OpenSimplexNoise.new(),
		'grass': OpenSimplexNoise.new()
	}
	
	var noise_seed = {
			'low': seed_key.hash() + "low".hash(),
			'high': seed_key.hash() + "high".hash(),
			'tree': seed_key.hash() + "tree".hash(),
			'grass': seed_key.hash() + "grass".hash()
	}
	if seed_key == "":
		randomize()
		noise_seed.low = randi()
		noise_seed.high = randi()
		noise_seed.tree = randi()
		noise_seed.grass = randi()
	
	noise.high.period = 50
	noise.high.octaves = 6
	noise.high.persistence = 0.5
	noise.high.lacunarity = 1.8
	noise.high.seed = noise_seed.high

	noise.low.period = 170 # more flat
	noise.low.octaves = 1
	noise.low.persistence = 0.1
	noise.low.lacunarity = 0.1
	noise.low.seed = noise_seed.low

	noise.tree.period = 10
	noise.tree.octaves = 1
	noise.tree.persistence = 2
	noise.tree.lacunarity = 0.1
	noise.tree.seed = noise_seed.low
	
	noise.grass.period = 10
	noise.grass.octaves = 1
	noise.grass.persistence = 2
	noise.grass.lacunarity = 0.1
	noise.tree.seed = noise_seed.grass
	
func save_chunk_as_file(chunk_path, scene_instance):
	var packed_scene = PackedScene.new()
	packed_scene.pack(scene_instance)
	var error = ResourceSaver.save(chunk_path, packed_scene)

func load_chunk_from_generator(chunk_path, chunk):
	var scene_instance = _generate_chunk_mesh(chunk.instance, chunk.position)
	save_chunk_as_file(chunk_path, scene_instance)

func load_chunk_from_file(chunk_path, chunk):
	var load_chunk = load(chunk_path)
	if load_chunk != null:
		chunk.instance = load_chunk.instance()
		map.add_child(chunk.instance)
	else:
		load_chunk_from_generator(chunk_path, chunk)

func spawn_chunk(chunk):
	chunk.instance.transform.origin = Vector3(chunk.position.y * map_size, 0, chunk.position.x * map_size)
	var chunk_path = cache_folder + "chunk" + str(chunk.position.x) + str(chunk.position.y) + ".tscn"
	var exists = ResourceLoader.exists(chunk_path)
	if !exists:
		load_chunk_from_generator(chunk_path, chunk)
	else:
		load_chunk_from_file(chunk_path, chunk)

func check_if_loaded(position):
	for chunk in loaded_chunks:
		if chunk.position.x == position.x && chunk.position.y == position.y:
			return true
	for chunk in loading_chunks:
		if chunk.position.x == position.x && chunk.position.y == position.y:
			return true
	return false
	
func check_if_removed(position):
	for chunk in unloading_chunks:
		if chunk.position.x == position.x && chunk.position.y == position.y:
			return true
	return false

func load_queued_chunks():
	var chunk = loading_chunks.pop_front()
	if chunk != null && !check_if_removed(chunk.position):
		var start_time = OS.get_system_time_msecs()
		spawn_chunk(chunk)
		map.call_deferred("add_child", chunk.instance)
		loaded_chunks.append(chunk)
		load_time = OS.get_system_time_msecs() - start_time

func unload_queued_chunks():
	var unchunk = unloading_chunks.pop_front()
	if unchunk != null:
		map.call_deferred("remove_child", unchunk.instance)
		unchunk.instance.queue_free()
		loaded_chunks.erase(unchunk)

func chunk_loader(userdata):
	while(true):
		load_queued_chunks()
		unload_queued_chunks()

func queue_unused_chunks(position : Vector2, size : int):
	for chunk in loaded_chunks:
		if abs(position.x - chunk.position.x) >= size || abs(position.y - chunk.position.y) >= size:
			if !check_if_removed(chunk.position):
				unloading_chunks.append(chunk)

func queue_chunks(position : Vector2, size : int):
	for i in range(size):
		for j in range(size):
			var loading_position = Vector2(position.x + i - int(size/2), position.y + j - int(size/2))
			if !check_if_loaded(loading_position):
				var new_chunk = {
					'instance': MeshInstance.new(),
					'position': Vector2(loading_position.x, loading_position.y)
				}
				loading_chunks.append(new_chunk)

func spawn_chunk_block(position: Vector2, size : int):
	queue_unused_chunks(position, size)
	queue_chunks(position, size)

func get_chunk_position(global_position : Vector3):
	var chunk_position = Vector2()
	chunk_position.x = int((global_position.z + (map_size/2 * sign(global_position.z))) / map_size)
	chunk_position.y = int((global_position.x + (map_size/2 * sign(global_position.x))) / map_size)
	return chunk_position


