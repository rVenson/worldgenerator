extends Spatial

var movement = Vector3()
var yaw = 0
var zoom = 0
var last_chunk = Vector2(-90, -90)
onready var world = get_node("/root/WorldGenerator")
export var field_of_view = 3
export var speed = 2

var offset = Vector3(0, 10.0, 0)

func _process(delta):
	if Input.is_action_pressed("ui_left"):
		movement.x = -1
	if Input.is_action_pressed("ui_right"):
		movement.x = 1
	if Input.is_action_pressed("ui_up"):
		movement.z = -1
	if Input.is_action_pressed("ui_down"):
		movement.z = 1
	if Input.is_action_pressed("ui_page_down"):
		movement.y = -1
	if Input.is_action_pressed("ui_page_up"):
		movement.y = 1
	if Input.is_action_pressed("rotate_left"):
		yaw = 1
	if Input.is_action_pressed("rotate_right"):
		yaw = -1
		
	world.UI.get_node("VBoxContainer/Position").text = "Position: " + str(transform.origin)
	world.UI.get_node("VBoxContainer/ChunkPosition").text = "Chunk: " + str(last_chunk)
	
	transform.origin += transform.basis * movement * delta * speed * 20
	rotate(Vector3.UP, yaw * delta * speed)
	offset.y += movement.y * delta * speed * 20
	movement = Vector3()
	yaw = 0
	
	# CAP OFFSET
	offset.y = clamp(offset.y, 10, 50)
	
	var chunk_position = world.get_chunk_position(transform.origin)
	if last_chunk != chunk_position:
		world.spawn_chunk_block(chunk_position, field_of_view)
		last_chunk = chunk_position
		
	# RAYCAST
	if $RayCast.is_colliding():
		var distance = transform.origin.y - $RayCast.get_collision_point().y - offset.y
		#transform.origin.y = $RayCast.get_collision_point().y + offset.y
		transform.origin.y -= distance * 0.1
		
	# CAP y
	transform.origin.y = clamp(transform.origin.y, 1, 50)
